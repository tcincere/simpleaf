#!/bin/bash
# Simple arch fetch (simpleaf) - fetch system information, simply.
# by: tcincere [https://gitlab.com/tcincere]

# Credit:
# Based on ufetch: https://github.com/jschx/ufetch 


# OS, shell and pacman information.
user="$(whoami)"
distro=$(awk -F "=" 'NR == 1 {gsub("\"",""); print tolower($2)}' /etc/os-release)
host="$(echo $HOSTNAME)"
kernel="$(uname -r)"
packages_pacman="$(pacman -Q | wc -l)"
shell="$(echo $SHELL)"

# Yay is quite slow...
#packages_yay="$(pacman -Qm | wc -l)"  

# Memory
memory_total=$(vmstat -s | head -n 2 | awk '$3 == "total" {print $1; exit(0)}' | numfmt --from-unit=K --to=iec)
memory_use=$(vmstat -s | head -n 2 | awk '$3 == "used" {print $1; exit(0)}' | numfmt --from-unit=K --to=iec)
memory_idle=$(vmstat -s | sed -n 5p | awk '{print $1}' | numfmt --from-unit=K --to=iec)

# Date and time.
uptime=$(uptime -p | sed 's/up //')
date_var=$(date +"%d/%m/%Y")
time=$(date +%H:%M)

# Colours.
bold="$(tput bold)"
cyan="$(tput setaf 6)"
white="$(tput setaf 7)"
magenta="$(tput setaf 5)"
reset="$(tput sgr0)"
blue="$(tput setaf 4)"

info_type="${reset}${bold}${magenta}"
info_info="${reset}${bold}${white}"
arrow="${bold}${cyan}->"

cat <<EOF

${info_type}    ___      ${info_info}simpleaf ${cyan}    ${date_var}
${info_type}   /\  \     user     $arrow  ${info_info}${user} (@${host})    
${info_type}  /::\  \    distro   $arrow  ${info_info}${distro}
${info_type} /::\:\__\   kernel   $arrow  ${info_info}${kernel}
${info_type} \/\::/  /   packages $arrow  ${info_info}${packages_pacman}
${info_type}   /:/  /    shell    $arrow  ${info_info}${SHELL}
${info_type}   \/__/     memory   $arrow  ${info_info}${memory_use}/${memory_total}, ${memory_idle} (idle)
${info_type}             up(time) $arrow  ${info_info}${uptime} (${time}) ${reset}

EOF