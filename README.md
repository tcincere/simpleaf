# Simpleaf (Simple arch fetch)

---

A simple arch fetch for GNU/Linux. This can be used on arch-based distributions such as Manjaro, in addition.

The fetch displays the following:

- Current user

- The distribution (/etc/os-release)

- Kernel version

- The number of pacman packages. There is also a variable set for yay packages, however, it causes the fetch to become a little sluggish. 

- The current shell

- In use, total and idle memory.

- Uptime of the machine, and time of day.

---

**Note:** The image below displays pink in comparison to the colour variable set as purple. This is due to my kitty colour configuration.

![](simpleaf.png)


